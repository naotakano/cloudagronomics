**Convolutional Neural Network**

- CNN.zip       CPU version.  Developed in Visual Studio.
- CNN_Src.tar   GPU version.  Developed with g++ and cuDNN.



There is no documentation on how to run it, but basically you can
modify the parameter file param_CNN_1.ini.  It creates a network
similar to AlexNet.

I wrote these about 4 years ago, and techniques such as Dropout and
Batch Normalization are not implemented.


**Graph Theory**

- GraphProject.zip (Visual Studio)

This is based on
https://www.sciencedirect.com/science/article/pii/S0012365X10001524

It is an unsolved and NP-Complete problem and I made an attempt to find an answer in a brute-force manner.

